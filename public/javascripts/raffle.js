$(document).ready(function() {

});

function raffle() {
  input = numbers[getRandomInt(numbers.length)];
  var $scramble = $("#output");
  $scramble.text(input);
  $scramble.scramble(10000, 50, "numbers", true);  
  $('.container .result').html('<div class="col-md-6 col-sm-12"><div class="card text-center"><div class="card-header"><i class="fas fa-spinner fa-pulse"></i></div></div></div>');
  setTimeout(function(){$.post('/getwinner', {'empid': input  }, function(data){
    console.log(data.length);
    $('.container .result').html(data);
  })},11000);
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}