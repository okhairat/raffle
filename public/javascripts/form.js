'use strict'

$(document).ready(function() {
    var $form=$('#register-form');
    var $name=$('input[name=name]');
    var $empid=$('input[name=empid]');
    $form.submit(function(e) {
        $('#register-form .alerts').html('')
        $('.form-control').each(function(){
            if($(this).val().length == 0)
            {
                console.log('show invalid');
                $(this).parent().find('.invalid-feedback').show();
            }
        });

        var $nonempty = $('.form-control').filter(function() {
            return this.value != ''
          });

        if($nonempty.length == 2) {
            // var $cardsWrapper = $('.results .cards-wrapper');
            // var $cardsLoader = $('.results .cards-loader');
            $.post('/register', { 
                'name': $name.val(),
                'empid': $empid.val()
             }, function(data){
                console.log(data.length);
                $('#register-form .alerts').html(data);
            }).fail(function(data){
                console.log(data.length);
                $('#register-form .alerts').html(data.responseText);
            });

            $name.val('');
            $empid.val('');
        }
        
        e.preventDefault();
    });

    $('.form-control').on('input', function () {
        $('.invalid-feedback').hide();
    });
});