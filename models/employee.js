var path = require('path');
const Sequelize = require('sequelize');
var dateFormat = require('dateformat');

const dbPath = path.resolve(__dirname, 'employees.db')
let connection = new Sequelize('', '', '', {
    host: 'localhost',
    dialect: 'sqlite',
    operatorsAliases: false,
    // SQLite only
    storage: dbPath,
});

connection.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
});

String.prototype.splice = function(idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

function isUAEmobile(val) {
    str = ''+val;
    str = str.replace(/\s/g,'').replace(/-/g,'');
    if(str.startsWith('009715') && str.length == 14) {
        return str.slice(2);
    } else if(str.startsWith('+9715') && str.length == 13) {
        return str.slice(1);
    } else if(str.startsWith('9715') && str.length == 12) {
        return str;
    } else if(str.startsWith('05') && str.length == 10) {
        return '971'+str.slice(1);
    } else if(str.startsWith('5') && str.length == 9) {
        return '971'+str;
    } else {
        return false;
    }
}

let Employee = connection.define('employee', {
    id: { 
        type: Sequelize.INTEGER, 
        autoIncrement: true ,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    employeeID: {
        type: Sequelize.INTEGER(15).UNSIGNED,
        allowNull: false,
        validate: {
            isNumeric: { msg: "Must be an integer"},
        }
    }
  },{
    timestamps: true
});

// force: true will drop the table if it already exists
Employee.sync({force: false}).then(() => {

  });


connection.sync({logging: console.log});
module.exports = Employee;