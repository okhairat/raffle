var express = require('express');
var router = express.Router();
let Employee = require('../models/employee');
const validator = require('validator');
const Sequelize = require('sequelize');

function isUAEmobile(str) {
  str = str.replace(/\s/g,'');
  if(str.startsWith('009715') && str.length == 14) {
    return str.slice(2);
  } else if(str.startsWith('+9715') && str.length == 13) {
    return str.slice(1);
  } else if(str.startsWith('9715') && str.length == 12) {
    return str;
  } else if(str.startsWith('05') && str.length == 10) {
    return '971'+str.slice(1);
  } else if(str.startsWith('5') && str.length == 9) {
    return '971'+str;
  } else {
    return false;
  }
}

function getAllMobileformats(str) {
  if(str.startsWith('9715') && str.length == 12) {
    return [str, '0'+str.slice(3), str.slice(3)];
  }
}

function isEID(str) {
  str = str.replace(/\s/g,'').replace(/-/g,'');
  if(str.startsWith('784') && str.length == 15) {
    return str;
  }
  return false;
}

/* GET home page. */
router.get('/', function(req, res, next) {

  res.render('form', { title: 'Event'});
});

router.post('/register', async function(req, res, next) {
  name = req.body.name;
  empid = req.body.empid;
  name_str = await name.replace(/\s/g,'').replace(/-/g,'');
  console.log(name_str);
  if((await validator.isAlpha(name_str)) || (await validator.isAlpha(name_str, ["ar-AE"]))) {
    if(await validator.isNumeric(empid.trim())) {
      Employee.count({ where: {employeeID: empid.trim()} }).then(function(count){
        if(count == 0) {
          console.log('Creating employee...');
          Employee.create({
            name: name.trim(),
            employeeID: empid.trim()
          });
          res.status(200);
          res.render('form-alert', { 
                    error: true, 
                    msgTitle: 'Thanks for participating, '+name.trim(), 
                    alertType: 'success', 
                    icon: 'fa-check-circle',
                    msg: 'You have been added successfully, Good luck!'}, function(err, alert){
                      res.send(alert);
                    });         
        } else {
          console.log('count is: '+count);
          console.log('Employee already exists!');
          res.status(400);
          res.render('form-alert', { 
                    error: true, 
                    msgTitle: 'Employee record exists', 
                    alertType: 'danger',
                    icon: 'fa-exclamation-triangle',
                    msg: 'Details already exists, are you sure you are adding a new record ?!'}, function(err, alert){
                      res.send(alert);
                    });
        }
      });
    } else {
      console.log('Employee ID not valid!');
      res.status(400);
      res.render('form-alert', { 
                error: true, 
                msgTitle: 'Employee ID not valid', 
                alertType: 'danger',
                icon: 'fa-exclamation-triangle',
                msg: 'Employee ID is not a valid number, please input valid ID number'}, function(err, alert){
                  res.send(alert);
                });
    }
  } else {    
      res.status(400);
      res.render('form-alert', { 
                error: true, 
                msgTitle: 'Not valid name!', 
                alertType: 'danger',
                icon: 'fa-exclamation-triangle',
                msg: 'please input a valid firstname and lastname either in Arabic or English.'}, function(err, alert){
                  res.send(alert);
                });
  }
});

router.get('/raffle', async function(req, res, next) {
  employees = await Employee.findAll();
  empids = [];
  employees.forEach(employee => {
    empids.push(employee.employeeID);
  });
  console.log(empids);
  res.render('raffle', { title: 'Raffle', data: empids});
});

router.post('/getwinner', async function(req, res, next){
  empid = req.body.empid;
  console.log('Empid is: '+empid);
  Employee.findOne({ where: {employeeID: empid} }).then(employee => {
    res.status(200);
    res.render('winner', { 
              winner: true, 
              EmpName: employee.name}, function(err, winner){
                res.send(winner);
              });

  })
});

module.exports = router;
